package ru.test.document;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import ru.test.document.question.Question;

import java.util.List;

public class WindowHelper {

    public static void initQuestionBar(HBox box, List<Question> questionList) {
        box.getChildren().clear();
        for (int i = 1; i <= questionList.size(); i++) {
            Button button = new Button(i + "");
            int finalI = i;
            button.setOnMouseClicked(MouseEvent -> showQuestionOnScene(box.getScene(),questionList.get(finalI - 1)));
            button.setId("questionBar"+(i-1));
            box.getChildren().add(button);
        }
        showQuestionOnScene(box.getScene(),questionList.get(0));
    }

    public static void showQuestionOnScene(Scene scene, Question question){
        Controller.setQuestionOnWindow(scene,question);
    }

}
