package ru.test.document.question;

import java.util.Arrays;

public class ManyQuestion implements Question {

    private String question;
    private String[] trueAnswer;
    private String[] falseAnswer;

    public ManyQuestion(String question, String[] trueAnswer, String... falseAnswer) {
        this.question = question;
        this.trueAnswer = trueAnswer;
        this.falseAnswer = falseAnswer;
    }

    @Override
    public String getQuestion() {
        return question;
    }


    public String[] getTrueAnswer() {
        return trueAnswer;
    }

    public String[] getFalseAnswer() {
        return falseAnswer;
    }

    @Override
    public int getCountTrueAnswer() {
        return trueAnswer.length;
    }

    @Override
    public String toString() {
        return Arrays.toString(trueAnswer);
    }
}
