package ru.test.document;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.test.document.question.ManyQuestion;
import ru.test.document.question.QuestionManager;
import ru.test.document.question.SingleQuestion;

//Порядок разработки нормативных документов
public class Main extends Application {

    public static QuestionManager questionManager = new QuestionManager();
    public static Session session = new Session("");
    public static Parent templateReport;
    @Override
    public void start(Stage primaryStage) throws Exception {
        templateReport = FXMLLoader.load(getClass().getResource("form/Report.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("form/sample.fxml"));
        Scene scene = new Scene(root, 600, 370);
        primaryStage.setScene(scene);
        Controller controller = new Controller();
        primaryStage.setOnShowing(controller::onShow);
        primaryStage.show();
    }


    public static void main(String[] args) {
        initQuestion();
        session.initQuestions();
        launch(args);
    }


    private static void initQuestion() {

        questionManager.add(new SingleQuestion("Что не является стадией разработки стандарта?",
                "Ввод стандарта в тестовом режиме"
                , "Разработка и утверждения технического задания на разработку стандарта", "Обработка отзывов", "Утверждение стандарта"));

        questionManager.add(new SingleQuestion("Что прилагается к каждому экземпляру проекта стандарта?",
                "Сводка отзывов по проекту стандарта",
                "График выполнения работ",
                "Список организаций подавших заявку на разработку стандарта",
                "Нужен только проект стандарта"));
        questionManager.add(new ManyQuestion("Что содержится в пояснительной записке к стандарту?",
                new String[]{"Предполагаемый срок введения стандарта в действие, срок действия и проверки", "Взаимодействие с другими стандартами"},
                "Экономические затраты на разработку стандарта", "Характеристики предприятий которые будут использовать разрабатываемый стандарт"));


        questionManager.add(new SingleQuestion(
                "Кто проводит экспертизу стандартов?",
                "Госстандарт РФ",
                "Институт государственной стандартизации",
                "Предприятия подавшие заявки на разработку стандарта",
                "Независимая, коммерческая организация выбранная Государством",
                "Не известно, информация об экспертах скрывается"
        ));

        questionManager.add(new SingleQuestion("Какое время дается на отправку отзывов на стандарт компании разработчику?",
                "30 дней",
                "60 дней",
                "14 дней",
                "7 дней"));

        questionManager.add(
                new ManyQuestion("Цели и задачи нормоконтроля?",
                        new String[]{"Правильность выполнения конструкторских документов в соответствии с требованиями стандартов",
                                "Соблюдение в разрабатываемых изделиях норм и требований"}
                        , "Соблюдение в разрабатываемых изделиях требований, установленных в государственных, отраслевых, республиканских стандартах и стандартах предприятий"));

        questionManager.add(new ManyQuestion("Кто может утвердить ТЗ на разработку стандарта?",
                new String[]{"Руководитель министерства разработчика", "Руководитель головной или базовой организации по стандартизации"},
                "Госстандарт РФ"));
        questionManager.add(new SingleQuestion("Сколько дается времени на отправку обобщенного отзыва?", "14 дней", "30 дней",
                "60 дней",
                "7 дней"));
    }
}
