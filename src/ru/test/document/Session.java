package ru.test.document;

import ru.test.document.question.Question;

import java.util.Arrays;
import java.util.List;

import static ru.test.document.Main.questionManager;

public class Session {

    private int score = 0;
    private String nameUser;
    private List<Question> questionList;
    private Boolean[] answer;


    public Session(String name) {
        this.nameUser = name;
    }


    public void initQuestions() {
        this.questionList = questionManager.getRandomQuestions();
        this.answer = new Boolean[questionList.size()];
    }


    public boolean getResult(Question question) {
        int index = getIndexQuestion(question);
        return answer[index] != null && answer[index];
    }

    public List<Question> getSessionQuestion() {
        return this.questionList;
    }


    public Question getNextQuestion(Question searchedQuestion) {
        int i = getIndexQuestion(searchedQuestion)+1;
        System.out.println(i+1);
        return i >= questionList.size() ? questionList.get(0) : questionList.get(i);
    }

    public int getIndexQuestion(Question searchedQuestion) {
        for (int i = 0; i < questionList.size(); i++) {
            if (questionList.get(i).getQuestion().equalsIgnoreCase(searchedQuestion.getQuestion())) {
                return i;
            }
        }
        return 0;
    }

    public void confirmAnswer(boolean answer, Question question) {
        this.answer[getIndexQuestion(question)] = answer;
        System.out.println("Session Answer " + Arrays.toString(this.answer));
    }
}
