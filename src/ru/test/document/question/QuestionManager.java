package ru.test.document.question;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class QuestionManager {

    private List<Question> questions = new ArrayList<>();
    private Random random = new Random();

    public QuestionManager() {

    }


    public void add(Question question) {
        questions.add(question);
    }

    /**
     * Получение рандомного вопроса и удаление его из списка
     */
    public Question getRandomQuestion() {
        if (questions.size() == 0) return null;
        int numberQuestion = random.nextInt(questions.size());
        Question question = questions.get(numberQuestion);
        questions.remove(numberQuestion);
        return question;
    }

    public static List<String> getRandomAnswerArray(Question question) {
        List<String> stringsBuffer = new ArrayList<>();
        List<String> randomAnswer = new ArrayList<>();
        Random random = new Random();
        if (question instanceof SingleQuestion) {
            stringsBuffer.add(((SingleQuestion) question).getTrueAnswer());
        } else if (question instanceof ManyQuestion) {
            stringsBuffer.addAll(Arrays.asList(((ManyQuestion) question).getTrueAnswer()));
        }
        stringsBuffer.addAll(Arrays.asList(question.getFalseAnswer()));
        while (stringsBuffer.size() > 0) {
            int index = random.nextInt(stringsBuffer.size());
            randomAnswer.add(stringsBuffer.get(index));
            stringsBuffer.remove(index);
        }
        return randomAnswer;
    }

    public List<Question> getRandomQuestions() {
        if (questions.size() == 0) return null;
        List<Question> questionList = questions;
        List<Question> responseQuestionList = new ArrayList<>();
        while (questionList.size()>0) {
            int numberQuestion = random.nextInt(questionList.size());
            Question question = questionList.get(numberQuestion);
            questionList.remove(numberQuestion);
            responseQuestionList.add(question);
        }
        return responseQuestionList;
    }
}
