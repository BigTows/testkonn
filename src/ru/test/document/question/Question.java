package ru.test.document.question;

public interface Question {

    String getQuestion();
    String[] getFalseAnswer();
    int getCountTrueAnswer();
}
