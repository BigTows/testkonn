package ru.test.document.question;

public class SingleQuestion implements Question {
    private String question;
    private String trueAnswer;
    private String[] falseAnswer;

    public SingleQuestion(String question, String trueAnswer, String... falseAnswer) {
        this.question = question;
        this.trueAnswer = trueAnswer;
        this.falseAnswer = falseAnswer;
    }

    @Override
    public String getQuestion() {
        return question;
    }


    public String getTrueAnswer() {
        return trueAnswer;
    }

    public String[] getFalseAnswer() {
        return falseAnswer;
    }

    @Override
    public int getCountTrueAnswer() {
        return 1;
    }

    @Override
    public String toString() {
        return trueAnswer;
    }
}
