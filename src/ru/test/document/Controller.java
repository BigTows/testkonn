package ru.test.document;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import ru.test.document.question.Question;
import ru.test.document.question.QuestionManager;
import ru.test.document.question.SingleQuestion;

import java.util.ArrayList;
import java.util.List;

import static ru.test.document.Main.questionManager;
import static ru.test.document.Main.session;

public class Controller {

    private static Button nextQuestionButton;


    public void onShow(WindowEvent windowEvent) {
        if (windowEvent.getSource() instanceof Window) {
            Scene scene = ((Window) windowEvent.getSource()).getScene();
            nextQuestionButton = (Button) getNodeByName(scene, "submitAnswer");
            HBox box = getQuestionBar(scene);
            WindowHelper.initQuestionBar(box, session.getSessionQuestion());
        }
    }

    public static void setQuestionOnWindow(Scene scene, Question question) {
        nextQuestionButton.setVisible(true);
        VBox questionBox = getQuestionBox(scene);
        Label questionLabel = getQuestionLabel(scene);
        questionBox.getChildren().clear();
        questionLabel.setText(question.getQuestion());
        getInputElementsByQuestion(questionBox, question);
        nextQuestionButton.setOnMouseClicked(mouseEvent -> prepareAnswer(scene, question, questionBox));
    }


    private static void getInputElementsByQuestion(VBox questionBox, Question question) {
        List<String> randomAnswer = QuestionManager.getRandomAnswerArray(question);
        if (question instanceof SingleQuestion) {
            ToggleGroup toggleGroup = new ToggleGroup();
            for (int i = 0; i < randomAnswer.size(); i++) {
                RadioButton radioButton = new RadioButton(i + 1 + ") " + randomAnswer.get(i));
                radioButton.setWrapText(true);
                radioButton.setId(randomAnswer.get(i));
                questionBox.getChildren().add(radioButton);
                radioButton.setToggleGroup(toggleGroup);
            }
        } else {
            for (int i = 0; i < randomAnswer.size(); i++) {
                CheckBox radioButton = new CheckBox(i + 1 + ") " + randomAnswer.get(i));
                radioButton.setWrapText(true);
                radioButton.setId(randomAnswer.get(i));
                questionBox.getChildren().add(radioButton);
            }
        }
    }


    private static void prepareAnswer(Scene scene, Question question, VBox questionBox) {
        List<String> answer = new ArrayList<>();
        for (Node node : questionBox.getChildren()) {
            if (node instanceof RadioButton) {
                RadioButton radioButton = (RadioButton) ((RadioButton) node).getToggleGroup().getSelectedToggle();
                if (radioButton == null) {
                    new Alert(Alert.AlertType.INFORMATION, "Пожалуйста выберите ответ").show();
                    return;
                }
                answer.add(radioButton.getId());
                break;
            } else if (node instanceof CheckBox) {
                if (((CheckBox) node).isSelected()) {
                    answer.add(node.getId());
                }
            }
        }
        session.confirmAnswer(confirmAnswer(answer, question), question);
        getNodeByName(scene, "questionBar" + session.getIndexQuestion(question)).setStyle("-fx-background-color: rgba(111,223,23,0.61)");
        WindowHelper.showQuestionOnScene(scene, session.getNextQuestion(question));
    }

    private static boolean confirmAnswer(List<String> answers, Question question) {
        question.getFalseAnswer();
        for (String answer : answers) {
            for (String badAnswer : question.getFalseAnswer()) {
                if (badAnswer.equalsIgnoreCase(answer)) {
                    return false;
                }
            }
        }
        return question.getCountTrueAnswer() == answers.size();
    }

    private static void nextQuestion(Scene scene) {
        Question question = questionManager.getRandomQuestion();

        setQuestionOnWindow(scene, question);
    }


    private static VBox getQuestionBox(Scene scene) {
        return (VBox) getNodeByName(scene, "questionBox");
    }

    private static Label getScoreLabel(Scene scene) {
        return (Label) getNodeByName(scene, "scoreLabel");
    }

    private static Label getQuestionLabel(Scene scene) {
        return (Label) getNodeByName(scene, "questionLabel");
    }

    private static Button getSubmitButton(Scene scene) {
        return (Button) getNodeByName(scene, "submitAnswer");
    }

    private HBox getQuestionBar(Scene scene) {
        return (HBox) getNodeByName(scene, "questionBar");
    }


    public static Node getNodeByName(Scene scene, String name) {
        return scene.lookup("#" + name);
    }

    public void endWork(MouseEvent mouseEvent) {
        nextQuestionButton.setVisible(false);
        if (mouseEvent.getSource() instanceof Button) {
            Button button = (Button) mouseEvent.getSource();
            button.setVisible(false);
            VBox vBox = getQuestionBox(button.getScene());
            vBox.setLayoutY(0);
            vBox.getChildren().clear();
            vBox.setMinWidth(400);
            getQuestionBar(button.getScene()).setVisible(false);
            getQuestionLabel(button.getScene()).setVisible(false);
            int countTrue = 0;
            for (Question question: session.getSessionQuestion()){
                boolean result =session.getResult(question);
                if (result){
                    countTrue++;
                }
                Label label = new Label(question.getQuestion()+" \nПравильно: "+(result?"Да":"Нет, правильный ответ: "+question.toString()));
                label.setMinHeight(80);
                label.setWrapText(true);
                vBox.getChildren().add(label);
            }

            button.getScene().getWindow().setHeight(1000);
            getScoreLabel(button.getScene()).setText(String.valueOf(countTrue));
        }
    }
}
